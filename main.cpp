/*
	*
	* This file is a part of QMake2CMake.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QCoreApplication>
#include "converter.h"

int main(int argc, char** argv) {

    QCoreApplication app(argc, argv);

    Converter *qm2cm = new Converter();
    QObject::connect( qm2cm, &Converter::finished, &app, QCoreApplication::quit );

    qm2cm->setBaseDir( argv[ 1 ] );
    qm2cm->start();

    return app.exec();
}

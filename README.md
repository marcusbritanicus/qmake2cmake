### QMake2CMake
This project is based on [QMake2CMake](https://github.com/diegostamigni/qmake2cmake.git) modified for use with CoreApps and Paper Projects

In order to build and install this, do the following:

```bash
git clone https://gitlab.com/marcusbritanicus/qmake2cmake.git
cd qmake2cmake
mkdir build && cd build
CMAKE_PREFIX_PATH=<QT INSTALLATION DIR> cmake ..
make -kj$(nproc) && sudo make install
```

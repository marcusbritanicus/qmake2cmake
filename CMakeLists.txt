project( QMake2CMake )
cmake_minimum_required( VERSION 3.1 )

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )

add_definitions ( -Wall )
set( CMAKE_INSTALL_PREFIX /usr )

find_package(Qt5Core REQUIRED)

set( CMAKE_AUTOMOC ON )

add_executable( qm2cm main.cpp converter.cpp converter.h )
target_link_libraries( qm2cm Qt5::Core )

install( TARGETS qm2cm DESTINATION bin )

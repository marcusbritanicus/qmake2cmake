/*
	*
	* This file is a part of QMake2CMake.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#ifndef CONVERTER_H
#define CONVERTER_H

#include <QThread>
#include <QStringList>


class Converter : public QThread
{
    Q_OBJECT
protected:
    virtual void run();

public:
    void setBaseDir(QString basis);

private:
    QStringList Files,
                CONFIG,QT,DEFINES,SOURCES,HEADERS,FORMS,DISTFILES,INCLUDEPATH,
                LIBS,RESOURCES,SUBDIRS,TRANSLATIONS;

    QString     DESTDIR,TEMPLATE,TARGET;

    void initQMakeVars();
    bool parseProFile(QString proFile);
    void operateOnVar(QStringList*& var, int &op, QString val);
    void writeCMakeLists(QString proFile);
signals:
    void progress(qreal completion, QString action);
};

#endif // CONVERTER_H

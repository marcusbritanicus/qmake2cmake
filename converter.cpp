/*
	*
	* This file is a part of QMake2CMake.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "converter.h"
#include <QDir>
#include <QFile>
#include <QRegExp>
#include <QTextStream>
#include <QVersionNumber>

#include <QCoreApplication>

void Converter::run() {

    qreal max = Files.count() * 3, now=0;
    foreach( QString file2do,Files ) {
        emit progress( now/max,"parse "+file2do );now += 1;
        if ( parseProFile( file2do ) ) {
            writeCMakeLists( file2do );
        } else {emit progress( now/max,"nix damit: "+file2do );now += 2;}
    }
}


void Converter::setBaseDir( QString basis ) {

    // What are we going to do?
    // We'll scan the basedir for the *.pro file, read it and add the content
    // to our Files list.  If it contains subdirs, we'll also add them here...
    QStringList dirs( basis ),liste;
    QString string;
    QDir base;
    QRegExp templ( "\\s*TEMPLATE\\s*[+]?=(.*)" );
    QRegExp subre( "\\s*SUBDIRS\\s*[+]?=(.*)", Qt::CaseSensitive,QRegExp::RegExp2 );
    int i;

    base.setFilter( QDir::Files | QDir::Readable );
    base.setNameFilters( QStringList() << "*.pro" );

    while( dirs.count() ) {
        base.setPath( dirs.first() );
        liste = base.entryList();
        if ( liste.count() > 0 ) {
            Files << QString( "%1/%2" ).arg( dirs.first() ).arg( liste.first() );
            QFile dieses( Files.last() );
            dieses.open( QIODevice::ReadOnly );
            string = dieses.readAll();
            dieses.close();
            string.remove( "\\\n" );
            liste = string.split( '\n' );
            i = liste.indexOf( templ );
            if ( i > -1 ) {
                string = templ.cap( 1 );
                if ( string.contains( "subdirs",Qt::CaseInsensitive ) ) {
                    i = liste.indexOf( subre );
                    if ( i > -1 ) {
                        string = subre.cap( 1 ).simplified();
                        if ( string.contains( '"' ) ) {
                            liste = string.split( '"',Qt::SkipEmptyParts );
                            for( i=0;i<liste.count();i++ )
                                liste[i] = liste[i].trimmed();
                            liste.removeAll( "" );
                        }

                        else {
                            liste = string.split( QRegExp( "\\s+" ),Qt::SkipEmptyParts );
                        }
                        foreach( string, liste )
                            dirs.append( QString( "%1/%2" ).arg( dirs.first() ).arg( string ) );
                    }
                }
            }
        }
        dirs.takeFirst();
    }
}

void Converter::initQMakeVars() {
    DESTDIR = "";
    CONFIG = QStringList() << "qt" << "warn_on" << "release" << "incremental" << "link_prl";
    QT = QStringList() << "core" << "gui";
    DEFINES.clear();
    SOURCES.clear();
    HEADERS.clear();
    FORMS.clear();
    DISTFILES.clear();
    INCLUDEPATH.clear();
    LIBS.clear();
    RESOURCES.clear();
    SUBDIRS.clear();
    TARGET.clear();
    TEMPLATE.clear();
    TRANSLATIONS.clear();
}

bool Converter::parseProFile( QString proFile ) {

    QFile file( proFile );
    QStringList lines;
    QString work;
    QRegExp liner( "^\\s*(\\w+)\\s*([-+]?=)\\s*(.*)$", Qt::CaseInsensitive, QRegExp::RegExp2 );
    int op; QStringList *sl;

    if ( not file.open( QIODevice::ReadOnly ) )
        return false;

    work = file.readAll();
    file.close();
    // make multilines simple
    work.remove( "\\\n" );
    // get the lines
    lines = work.split( '\n',Qt::SkipEmptyParts );

    initQMakeVars();
    // now parse the lines ...
    while( lines.count() ) {
        // capture all the contents...
        lines.first().indexOf( liner );
        // cap( 1 ) is the variable, cap( 2 ) is the operator
        if ( liner.captureCount() > 0 ) {
            if ( liner.cap( 1 ) == "TEMPLATE" )
                TEMPLATE = liner.cap( 3 );

            else if ( liner.cap( 1 ) == "TARGET" )
                TARGET = liner.cap( 3 );

            else if ( liner.cap( 1 ) == "DESTDIR" )
                DESTDIR = liner.cap( 3 );

            else {
                sl = 0;
                if ( liner.cap( 2 ) == "-=" )
                    op = -1;

                else if ( liner.cap( 2 ) == "+=" )
                    op = +1;

                else op = 0;

                if ( liner.cap( 1 ) == "CONFIG" )
                    sl = &CONFIG;

                else if ( liner.cap( 1 ) == "QT" )
                    sl = &QT;

                else if ( liner.cap( 1 ) == "DEFINES" )
                    sl = &DEFINES;

                else if ( liner.cap( 1 ) == "SOURCES" )
                    sl = &SOURCES;

                else if ( liner.cap( 1 ) == "HEADERS" )
                    sl = &HEADERS;

                else if ( liner.cap( 1 ) == "FORMS" )
                    sl = &FORMS;

                else if ( liner.cap( 1 ) == "DISTFILES" )
                    sl = &DISTFILES;

                else if ( liner.cap( 1 ) == "INCLUDEPATH" )
                    sl = &INCLUDEPATH;

                else if ( liner.cap( 1 ) == "LIBS" )
                    sl = &LIBS;

                else if ( liner.cap( 1 ) == "RESOURCES" )
                    sl = &RESOURCES;

                else if ( liner.cap( 1 ) == "SUBDIRS" )
                    sl = &SUBDIRS;

                else if ( liner.cap( 1 ) == "TRANSLATIONS" )
                    sl = &TRANSLATIONS;

                if ( sl )
                    operateOnVar( sl,op,liner.cap( 3 ) );
            }
        }
        else {
            //TODO: write something complicated here ...
        }

        lines.takeFirst();  //just that the loop won't be infinite...
    }

    return true;
}

void Converter::operateOnVar( QStringList*& var, int &op, QString val ) {
    // First part: split value correctly
    // - it can contain quoted strings
    // - or it simply contains space-separated strings ...
    // -> make them all quoted for that purpose, split them, and remove quotes
    //    afterwards ...
    int i; QString v=val;
    i = 0;
    while( i < v.count() ) {
        while( ( i<v.count() ) && ( v[i].isSpace() ) )
            i++;

        if ( v[i] != '"' ) {
            v.insert( i++,'"' );
            while( ( i<v.count() ) && ( not v[i].isSpace() ) )
                i++;
            v.insert( i++,"\"+++" );
        }

        else {
            ++i;
            while( ( i<v.count() ) && ( v[i] != '"' ) )
                i++;

            v.insert( ++i,"+++" );
        }
        i+=3;
    }

    //2nd part: split string ...
    QStringList lst = v.split( QRegExp( "\"[+]{3}\\s*\"?" ),Qt::SkipEmptyParts );
    v = lst.takeFirst();
    v.remove( '"' );
    lst.insert( 0,v );

    v = lst.takeLast();
    v.remove( '"' );
    lst << v;

    //3rd part: operate on the variable...
    switch( op ) {
        case -1: {    //"-="
            foreach( QString item, lst )
                var->removeAll( item );

            break;
        }

        case 0: {
            var->clear();
        }

        case 1: {
            *var << lst;
            break;
        }
    }
}

void Converter::writeCMakeLists( QString proFile ) {
    /*  Let's start writing a CMakeLists.txt file ... */
    QString links, help, fn = proFile.section( '/', 0, -2 ) + "/CMakeLists.txt";
    QFile file( fn );
    QTextStream text( &file );

    if ( not file.open( QIODevice::WriteOnly ) )
        return;

    if ( not TARGET.isEmpty() )
        text << QString( "project( %1 )\n" ).arg( TARGET );

    else
        TARGET = proFile.section( '/',0,-2 ).section( '/',-1 );

    text << "cmake_minimum_required( VERSION 3.1 )\n\n";

    QVersionNumber version( 1, 0, 0 );
    if ( qApp->arguments().contains( "--version" ) ) {
        int idx = qApp->arguments().indexOf( "--version" );
        version = QVersionNumber::fromString( qApp->arguments().at( idx + 1 ) );
    }

    text << QString( "set( PROJECT_VERSION %1 )\n" ).arg( version.toString() );
    text << QString( "set( PROJECT_VERSION_MAJOR %1 )\n" ).arg( version.majorVersion() );
    text << QString( "set( PROJECT_VERSION_MINOR %1 )\n" ).arg( version.minorVersion() );
    text << QString( "set( PROJECT_VERSION_PATCH %1 )\n\n" ).arg( version.microVersion() );
    text << "set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )\n";
    text << "add_compile_definitions(VERSION_TEXT=\"${PROJECT_VERSION}\")\n\n";

    // Standard
    text << "set( CMAKE_CXX_STANDARD 17 )\n";
    text << "set( CMAKE_INCLUDE_CURRENT_DIR ON )\n";
    text << "set( CMAKE_BUILD_TYPE Release )\n\n";

    text << "add_definitions ( -Wall )\n";
    text << "if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )\n";
    text << "    set( CMAKE_INSTALL_PREFIX \"/usr\" CACHE PATH \"Location for installing the project\" FORCE )\n";
    text << "endif()\n\n";



    text << "set( CMAKE_INSTALL_PREFIX /usr )\n\n";

    // Qt5 Modules
    QString LIBS;
    if ( CONFIG.contains( "qt" ) ) {
        text << "set( CMAKE_AUTOMOC ON )\n";
        text << "set( CMAKE_AUTORCC ON )\n";
        text << "set( CMAKE_AUTOUIC ON )\n\n";

        if ( qApp->arguments().contains( "QtCore" ) ) {
            text << "find_package( Qt5Core REQUIRED )\n";
            LIBS += "Qt5::Core ";
        }

        if ( qApp->arguments().contains( "QtGui" ) ) {
            text << "find_package( Qt5Gui REQUIRED )\n";
            LIBS += "Qt5::Gui ";
        }

        if ( qApp->arguments().contains( "QtWidgets" ) ) {
            text << "find_package( Qt5Widgets REQUIRED )\n";
            LIBS += "Qt5::Widgets ";
        }

        if ( qApp->arguments().contains( "QtDBus" ) ) {
            text << "find_package( Qt5DBus REQUIRED )\n";
            LIBS += "Qt5::DBus ";
        }

        if ( qApp->arguments().contains( "QtNetwork" ) ) {
            text << "find_package( Qt5Network REQUIRED )\n";
            LIBS += "Qt5::Network ";
        }

        if ( qApp->arguments().contains( "QtXml" ) ) {
            text << "find_package( Qt5Xml REQUIRED )\n";
            LIBS += "Qt5::Xml ";
        }

        if ( qApp->arguments().contains( "QtX11Extras" ) ) {
            text << "find_package( Qt5`X11Extras REQUIRED )\n";
            LIBS += "Qt5::X11Extras ";
        }

        if ( qApp->arguments().contains( "QtMultimedia" ) ) {
            text << "find_package( Qt5Multimedia REQUIRED )\n";
            LIBS += "Qt5::Multimedia ";
        }

        text << "\n";

        links.replace( "INCLUDE_DIR", "LIBRARY" );
    }

    if ( qApp->arguments().contains( "CoreApp" ) )
        LIBS += " cprime-widgets cprime-core";

    if ( not DESTDIR.isEmpty() ) {
        if ( TEMPLATE.contains( "app" ) )
            text << QString( "set( EXECUTABLE_OUTPUT_PATH %1 )\n" ).arg( DESTDIR );

        if ( TEMPLATE.contains( "lib" ) )
            text << QString( "set( LIBRARY_OUTPUT_PATH %1 )\n" ).arg( DESTDIR );
    }

    // check subdirs/app/lib and add directories/files accordingly
    if ( TEMPLATE.contains( "subdirs", Qt::CaseInsensitive ) ) {
        foreach( QString sd, SUBDIRS )
            text << QString( "add_subdirectory( %1 )\n" ).arg( sd );
    }

    else if ( TEMPLATE.contains( QRegExp( "(app)|(lib)" ) ) ) {
        //write the source/header/ui/moc-stuff

        //prepare helper
        help = QString( "${%1_SRCS}" ).arg( TARGET );

        // write headers
        text << QString( "set( HEADERS\n\t%1\n)\n\n" ).arg( HEADERS.join( "\n\t" ) );

        // write sources
        text << QString( "set( SOURCES\n\t%1\n)\n\n" ).arg( SOURCES.join( "\n\t" ) );


        // write forms
        if ( not FORMS.isEmpty() ) {
            text << QString( "set( UIS\n\t%2\n)\n\n" ).arg( TARGET ).arg( FORMS.join( "\n\t" ) );
            help += " ${UIS}";
        }

        // write resources
        if ( not RESOURCES.isEmpty() ) {
            text << QString( "set ( RESOURCES\n\t%2\n)\n\n" ).arg( TARGET ).arg( RESOURCES.join( "\n\t" ) );
            help += " ${RSCS}";
        }

        if ( TEMPLATE == "app" ) {
            text << QString( "add_executable( %1 " ).arg( TARGET );
        }

        else { //"lib" ( or even a plugin
            text << QString( "add_library( %1 " ).arg( TARGET );
            if ( CONFIG.contains( "plugin" ) )
                text << "MODULE ";

            else if ( CONFIG.contains( "static" ) )
                text << "STATIC ";

            else
                text << "SHARED ";
        }

        // Sources, UIs, RCCs, MOCs
        text << QString( "${SOURCES} " );
        if ( HEADERS.size() )
            text << QString( "${HEADERS} " );

        if ( FORMS.size() )
            text << QString( "$UIS} " );

        if ( RESOURCES.size() )
            text << QString( "${RESOURCES} " );

        text << " )\n";

        if ( TEMPLATE != "app" ) {
            text << QString( "set_target_properties( %1 PROPERTIES VERSION ${PROJECT_VERSION} )\n" ).arg( TARGET );
            text << QString( "set_target_properties( %1 PROPERTIES SOVERSION ${PROJECT_VERSION} )\n" ).arg( TARGET );
            text << QString( "set_target_properties( %1 PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR_MINOR} )\n" ).arg( TARGET );
            text << QString( "set_target_properties( %1 PROPERTIES SOVERSION ${PROJECT_VERSION_MAJOR} )\n" ).arg( TARGET );
        }

        text << QString( "target_link_libraries( %1 %2 %3" ).arg( TARGET ).arg( links ).arg( LIBS );
        for( QString arg: qApp->arguments() ) {
            if ( arg.startsWith( "LIBS+=" ) )
                text << arg.replace( "LIBS+=", " " );
        }
        text << QString( " )\n\n" );

        if ( TEMPLATE == "app" ) {
            text << QString( "install( TARGETS %1 DESTINATION bin )\n" ).arg( TARGET );
            text << QString( "install( PROGRAMS %1.desktop DESTINATION share/applications )\n" ).arg( TARGET );
            text << QString( "install( FILES %1.svg DESTINATION share/icons/hicolor/scalable/apps/ )\n" ).arg( TARGET );
        }

        else {
            text << QString( "install( TARGETS %1 EXPORT LIBRARY )\n" ).arg( TARGET );
            text << QString( "install( FILES ${HEADERS} DESTINATION include/$$$$$$$/ )\n" );
        }
    }

    text.flush();
    file.close();
}
